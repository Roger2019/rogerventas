<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Models\Proveedor;
use App\Permission\Models\Role;
use App\Permission\Models\Permission;
use App\Models\Categoria;
//use App\Models\Detalle_entrada_temp;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class PermissionInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //truncate tables -- elimina todo y reinicia todo para las tablas role_user y permission_role
        DB::statement('SET foreign_key_checks=0');
        DB::table('role_user')->truncate();
        DB::table('permission_role')->truncate();
        //DB::table('detalle_entrada_temp')->truncate();
        Permission::truncate();
        Role::truncate();
        Proveedor::truncate();
        Categoria::truncate();
        DB::statement('SET foreign_key_checks=1');
        //user admin
        $useradmin = User::where('email','admin@admin.com')->first();
        if ($useradmin) {
            $useradmin->delete();
        }
        $useradmin = User::create([
            'name' =>'admin', 
            'email' => 'admin@admin.com',
            'password' =>Hash::make('admin')
        ]);
        //rol admin
        $roladmin = Role::create([
            'name'=>'Admin',
            'slug'=>'Admin',
            'description'=>'Administrador',
            'full-access'=>'Yes'
        ]);
        //table role_user
        $useradmin->roles()->sync([$roladmin->id]);

        //permission
        $permission_all = [];
        //permisssion role
       //permisosos de los roles
 		$permission =  Permission::create([
            'name'=>'List role',
            'slug'=>'role.index',
            'description'=>'A user can list role'
        ]);

        $permission_all[] = $permission->id;

        //PERMISOS DEL MENU PRINCIPAL del administrador
        $permission =  Permission::create([
            'name'=>'listar el menu principal',
            'slug'=>'admin.index',
            'description'=>'un administrador puede ver menu'
        ]);

        $permission_all[] = $permission->id;
        //PERMISOS PARA EL MENU DE ALMACÉN
        $permission =  Permission::create([
            'name'=>'listar el menu de almacen',
            'slug'=>'almacen.index',
            'description'=>'Un usuario puede ver menu de alamcen'
        ]);
        $permission_all[] = $permission->id;
        //PERMISOS PARA EL MENU DE COMPRAS
        $permission =  Permission::create([
            'name'=>'listar el menu de compras',
            'slug'=>'compras.index',
            'description'=>'Un usuario puede ver menu de compras'
        ]);
        $permission_all[] = $permission->id;
        //PERMISOS PARA EL MENU DE VENTAS
        $permission =  Permission::create([
            'name'=>'listar el menu de ventas',
            'slug'=>'ventas.index',
            'description'=>'Un usuario puede ver menu de ventas'
        ]);
        $permission_all[] = $permission->id;

        /**table permision_role*/
        $roladmin->permissions()->sync($permission_all);
        /**comando para ejeutar el seeder*/
        //php artisan migrate --seed

        //comando para eliminar y crear todo de una sola vez   
        //php artisan migrate:refresh --seed
    }
}
