<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCapturarinventarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('capturarinventario', function (Blueprint $table) {
            $table->id('idcaptura');
            $table->foreignId('articulo_id')->references('idarticulo')->on('productos')->onDelete('cascade');
            $table->decimal('cantidad',11,3);
            $table->date('fecha');
            $table->time('hora', 0);
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('capturarinventario');
    }
}
