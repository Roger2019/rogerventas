
export class Search{
    constructor(myurl,mysearch,ul_add_li,idli){
        this.url = myurl;
        this.mysearch = mysearch;
        this.ul_add_li = ul_add_li;
        this.idli = idli;
    }

    Inputsearch(){
        this.mysearch.addEventListener("input", (e) =>{
            e.preventDefault();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr("content");
            let minimo_letras = 0; // minimo letras visibles en el autocompletar
            let valor = this.mysearch.value;
            if (valor.length > minimo_letras) {
                let datos = new FormData();
                datos.append("valor", valor);
                //console.log(valor);
                fetch(this.url, {
                    headers: {
                    'X-CSRF-TOKEN': CSRF_TOKEN// <--- aquí el token de seguridad.
                    },
                    method:'post',
                    body:datos
                })
                .then(data => data.json())
                .then(data => {
                    console.log('Success:', data);
                    this.Showli(data,valor);
                })
                .catch(function(error){
                    console.error('Error:', error)
                });
            }else{
                this.ul_add_li.style.display = "none";
            }
        });

    }

    Showli(data,valor){
        this.ul_add_li.style.display = "block";
        if (data.alldata != "") {
            let arr = data.alldata;
            this.ul_add_li.innerHTML="";
            let n = 0;
            if (data.modulo == "venta") {
                this.Clearcamposproductoventa();
                this.Showproductoventa(arr,valor,n);
                this.myonclickventa();
            }else if (data.modulo == "proveedor") {
                /**no aplica para el provedor esta function comentada*/
                //this.CLearcamposproveedor();
                this.Showproveedor(arr,valor,n);
                this.myonclickproveedor();
                

            }

            let adclasli= document.getElementById('1'+this.idli);
            adclasli.classList.add('selected');
        }else{
            switch (data.modulo) {
                case "venta":
                    this.Clearcamposproductoventa();
                break;
                case "proveedor":
                    this.CLearcamposproveedor();
                break;
                default:
                break;
            }
            this.ul_add_li.innerHTML="";
            this.ul_add_li.innerHTML +=`
                <li>Sin datos</li>
            `;
        }
    }
    Showproductoventa(arr,valor,n){
        for (let item of arr) {
            n++;
            let nombre = item.nombre;
            const array = `showproductoventa*/*/*${item.id}*/*/*${item.codigo}*/*/*${item.nombre}*/*/*${item.iva}*/*/*${item.venta}*/*/*${item.stock}*/*/*${item.descuento}`;
            this.ul_add_li.innerHTML +=`
            <li id="${n+this.idli}" value="${item.nombre}" name="${array}" class="list-group-item"  style="width:622px;border:1px solid #f1f1f1;">
                    <div class="d-flex flex-row " style="border:1px solid #ccd2db;margin-right:-17px;margin-top:-10px;margin-left:-19px;margin-bottom:-10px;">
                    <div class="p-2 text-center" style="border:1px solid #ccd2db;">
                        <img src="${item.img}" class="img-thumbnail" width="50" height="50" >
                    </div>
                    <div class="p-2">
                            <strong>${nombre.substr(0,valor.length)}</strong>
                            ${nombre.substr(valor.length)}
                            <p class="card-text">P. venta $ : ${item.venta}</p>
                    </div>
                    </div>
            </li>
            `;
        }
    }

    Showproveedor(arr,valor,n){
        for (let item of arr) {
            n++;
            let nombre = item.nombre;
            const array = `showproveedor*/*/*${item.id}*/*/*${item.nombre}`;
            this.ul_add_li.innerHTML+=`
                <li id="${n+this.idli}" value="${item.nombre}" name="${array}" class="list-group-item">
                    <div>
                        <strong>${nombre.substr(0,valor.length)}</strong>
                        ${nombre.substr(valor.length)}
                    </div>
                </li>
            `;
        }
    }

    InputKeydown(id_ul){
        this.mysearch.addEventListener("keydown", (e) =>{
            switch (e.keyCode) {
                case 40:
                    e.preventDefault(); // prevent moving the cursor
                    const nextkeycode = document.querySelector(id_ul+" li:not(:last-child).selected");
                    if (nextkeycode !=null) {
                        console.log(nextkeycode);
                        nextkeycode.classList.remove('selected');
                        //console.log(lisec);
                        const  nextli = nextkeycode.nextElementSibling;
                        nextli.classList.add('selected');
                        //console.log(nextli.className);
                    }
                    /*d.classList.add('selected');*/
                    //$('#autocompleteli li:not(:last-child).selected').removeClass('selected').next().addClass('selected');
                break;
                case 38:
                    e.preventDefault(); // prevent moving the cursor
                    const prevkeycode = document.querySelector(id_ul+" li:not(:first-child).selected");
                    if (prevkeycode != null) {
                        console.log(prevkeycode);
                        prevkeycode.classList.remove('selected');
                        const prevli = prevkeycode.previousElementSibling;
                        prevli.classList.add('selected');
                    }
                    /*$('#autocompleteli li:not(:first-child).selected').removeClass('selected')
                        .prev().addClass('selected');*/
                break;
                case 13:
                    /**se desabilita el envio del formulario*/
                    e.preventDefault();
                    console.log("se deshabiloto")
                    //const navbar = Array.from(document.querySelector('#autocompleteli>.selected'));
                    //console.log('Get first: ', navbar[0].textContent);
                    const liselected = document.querySelector(id_ul+'>.selected');
                    //const text = liselected.textContent;
                    const textarray = liselected.getAttribute('name');
                    //const livalue = liselected.value;
                    console.log(textarray);
                    const dividir = textarray.split('*/*/*'); // split string on comma space
                    console.log(dividir);
                    const validar = dividir[0];
                    switch (validar) {
                        case "showproductoventa":
                                console.log(validar);
                                this.Variablesproductoventa(dividir);
                                document.querySelector(id_ul).innerHTML = "";
                        break;
                        case "showproveedor":
                            console.log(validar);
                            this.Variablesproveedor(dividir);
                            document.querySelector(id_ul).innerHTML = "";
                            
                        break;
                        default:
                            break;
                    }
                    return false;
                break;
            }
        });
    }

    Variablesproductoventa(dividir){
        const getid = document.querySelector("#idarticulo").value = dividir[1];
        const getcodigo = document.querySelector("#CodigoArticulo").value = dividir[2];
        const getnombre = document.querySelector("#NombreArticulo").value = dividir[3];
        const getiva = document.querySelector("#iva").value = dividir[4];
        const getventa = document.querySelector("#pvprecio_venta").value = dividir[5];
        const getstock = document.querySelector("#pvstock").value = dividir[6];
        const getdescuento = document.querySelector("#pvdescuento").value = dividir[7];
        this.mysearch.value = dividir[3];
    }

    Clearcamposproductoventa(){
        const getid = document.querySelector("#idarticulo").value = "";
        const getcodigo = document.querySelector("#CodigoArticulo").value = "";
        const getnombre = document.querySelector("#NombreArticulo").value = "";
        const getiva = document.querySelector("#iva").value = "";
        const getventa = document.querySelector("#pvprecio_venta").value = "";
        const getstock = document.querySelector("#pvstock").value = "";
        const getdescuento = document.querySelector("#pvdescuento").value = "";
    }
    Variablesproveedor(dividir){
        const getidproveedor =  document.querySelector("#idproveedor").value = dividir[1];
        this.mysearch.value = dividir[2];
    }

    CLearcamposproveedor(){
        const getidproveedor =  document.querySelector("#idproveedor").value = "";
        this.mysearch.value = "";
    }

    
    myonclickventa(){
        let listItems = document.querySelectorAll("#autocompleteventa li");
        listItems.forEach((item, index) => {
            item.addEventListener('click', (event) => {
                const textarray = item.getAttribute('name');
                console.log(textarray);
                const dividir = textarray.split('*/*/*'); // split string on comma space
                console.log(dividir);
                this.Variablesproductoventa(dividir);
                this.ul_add_li.innerHTML = "";
            });
        });
    }   

    myonclickproveedor(){
        let listItems = document.querySelectorAll("#autocompleteli li");
        listItems.forEach((item, index) => {
            item.addEventListener('click', (event) => {
                const textarray = item.getAttribute('name');
                console.log(textarray);
                const dividir = textarray.split('*/*/*'); // split string on comma space
                console.log(dividir);
                this.Variablesproveedor(dividir);
                this.ul_add_li.innerHTML = "";
            });
        });
    }

}
