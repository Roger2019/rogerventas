/**GET DATA ON THE SALES OF PRODUCTS*/
$(document).ready( function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(function() {
    $('#ventas_table').DataTable({
            "paging": true,
            "autoWidth": false,
           processing: true,
           serverSide: true,
           ajax: {
             url:'/showlistventas',
            type: 'GET',
           },
           columns: [
                   { data: 'idventa', name: 'idventa'},
                   { data:'fecha_hora', name: 'fecha_hora'},
                   { data:'num_folio', name: 'num_folio'},
                   { data: 'tipo_comprobante', name: 'tipo_comprobante'},
                   { data: 'total_venta', name: 'total_venta' },
                   { data: 'estado', name: 'estado' },
                   {data: 'action', name:'action'}
                 ],
          order: [[0, 'desc']]
        });
    });
});
/*SALES CONSTRUCT*/
class VentasFun{
    constructor(name,id,idmodal){
        this.name= name;
        this.id = id;
        this.modal = idmodal;
    }
}
/**OPTIONS CONSTRUCT*/
class OptionsFunction{

    async ExtractFetch(options){
        try {
            console.log(options);
            let response = await fetch(options.name+options.id);
            let json = await response.json();
            console.log(json);
            this.PaintTable(json);
            let myModal = options.modal;
           this.ShowModalDetalle(myModal);

           
        } catch (error) {
            console.log("Ocurrio un error", error);
        }
    }
    ShowModalDetalle(myModal){
        const idmodaldetalleventa = document.querySelector(myModal);
        const myModaldetalleventa = new bootstrap.Modal(idmodaldetalleventa);

        myModaldetalleventa.show();
    }

    PaintTable(json){
        const art = json.detalles;
        let idtbody = document.querySelector("#show_details_sale");
        idtbody.innerHTML = "";
        for(let item of art){
            idtbody.innerHTML += `
                <tr>
                    <td>${item.articulo}</td>
                    <td>${item.cantidad}</td>
                    <td>${item.precio_venta}</td>
                    <td>${item.descuento}</td>
                    <td>${item.subtotal}</td>
                </tr>
            
            `;
        } 
        const dcli = document.querySelector("#detalle_cliente");
        dcli.innerHTML = json.result.nombre;
        const dtipo = document.querySelector("#detalle_tipo");
        dtipo.innerHTML = json.result.tipo_comprobante;
        const dfolio= document.querySelector("#detalles_folio");
        dfolio.innerHTML = json.result.num_folio;
        const dtotales = document.querySelector("#details_total_sale");
        dtotales.innerHTML = json.result.total_venta;
    }
}

/**FUNCION THAT SHOW THE DETAILS OF THE SALES */
function obtener_detalle_venta(id){
    //create a new Object Venta
    const options = new VentasFun("/venta-detalle/",id,"#ModalDetalleVenta");
    //create a new OptionsFUnction
    const showmodaldata = new OptionsFunction();
    showmodaldata.ExtractFetch(options);
    //user.ShowModalDetalle();

}