import {Search} from "../export_funcion/export_function search.js";
const mysearch = document.querySelector("#BuscarVentaProducto");
const ul_add_li = document.querySelector("#autocompleteventa");
const idli = "prodventa";
const myurl ="/findproducto";
const searchprodventa = new Search(myurl,mysearch,ul_add_li,idli);
searchprodventa.Inputsearch();
const id_ul = "#autocompleteventa";
searchprodventa.InputKeydown(id_ul);

/*AL REARGAR LA PAGINA SE EJECUTA LA CONSULTA QUE MUESTRA LA TABLA SI TIENE DATOS TEMPORALES*/
window.onload = ()  =>{
    mostrarventaproductostemp();
}
/**FUNCTIONN QUE PERMITE CONSULTARLOS PRODUCTOS AGREGADOS TEMPORALMENTE Y NO SE HAN VENDIDO*/
const mostrarventaproductostemp = () => {
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr("content");
  var id_user = document.getElementById("id_user").value;
  var url = "/showproductosventatemp";
  var datos = new FormData();
  datos.append('id_user',id_user);
    fetch(url,{
        headers: {
            'X-CSRF-TOKEN': CSRF_TOKEN// <--- aquí el token de seguridad.
        },
        method:'post',
        body:datos
    })
    .then(data => data.json())
    .then(data => {
     //   console.log('Success:', data);
        if (data.estado == 1) {
            pintar_tabla_venta(data);
        }else if (data.estado == 0) {
            mensaje_error_save_venta_temp(data);
        }
    })
    .catch(function(error){
        console.error('Error:', error)
    });

}
/*FUNCTION QUE PERMITE GUARDAR LOS PRDUCTOS EN LA TABLA TEMPORAL*/
const FormSaveProducto = document.querySelector("#save_producto_venta");
FormSaveProducto.addEventListener("submit", (e) => { 
    e.preventDefault();
    //alert("se ejcuto");
    const btnsavetempventprod = document.querySelector("#btn_add_prod_tem_vent");
    btnsavetempventprod.innerHTML = "";
    btnsavetempventprod.disabled = true;
    btnsavetempventprod.innerHTML += `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Enviando datos...`;
    const datosprodvent = new FormData(document.getElementById("save_producto_venta"));
    var url = "/saveprodtempvent";
    fetch(url,{
        method: "post",
        body:datosprodvent
    })
    .then(data => data.json())
    .then(data => {
        console.log("Success:", data);
        if (data.estado == 1) {
            pintar_tabla_venta(data);
            FormSaveProducto.reset();
            btnsavetempventprod.disabled = false;
            btnsavetempventprod.innerHTML = "Agregar";

        }else if (data.estado == 0) {
           mensaje_error_save_venta_temp(data);
            btnsavetempventprod.disabled = false;
            btnsavetempventprod.innerHTML = "Agregar";

        }else if (data.estado == "errorvalidacion") {
           mensaje_error_save_venta_temp(data);
            btnsavetempventprod.disabled = false;
            btnsavetempventprod.innerHTML = "Agregar";
        }
    })
    .catch(function(error) {
        console.error("Error:", error);

    });

})
/**MENSAJES DE ERRROR */
var mensaje_error_save_venta_temp = (data) => {
    var errores = document.querySelector(".print-save-error-msg");
    errores.innerHTML = "";
    errores.style.display = "block";
    const mensaje_validacion_ventas= data.mensaje;
    mensaje_validacion_ventas.forEach(element => {
        // console.log(element);
        errores.innerHTML += "<li>" + element + "</li>";
    });
    window.setTimeout(function() {
      const diverror =  document.querySelector(".print-save-error-msg");
      diverror.style.display="none";
    }, 3000);
};

/*FUNCTION QUE PINTA LOS DATOS OBTENIDOS DE LA TABLA TEMPORAL DE LAS VENTAS DE LOS PRODUCTOS*/
const pintar_tabla_venta = (data) => {
    const productos_venta_temp = data.productos;
    const pintar_tabla_producto_temp = document.querySelector("#tabla_venta_productos_temp");
    pintar_tabla_producto_temp.innerHTML = "";
    let count = 0; 
    for (let item of productos_venta_temp ) {
        count++;
        pintar_tabla_producto_temp.innerHTML +=`
        <tr>
        <td>${count}</td>
        <td><input type="hidden" name="idarticulo[]" value="${item.id_articulo}">${item.nombre}</td>
        <td><input type="number" class="size_input" readonly name="cantidad[]" value="${item.cantidad}"></td>
        <td><input type="number" class="size_input" readonly name="precio_venta[]" value="${item.precio}"></td>
        <td><input type="number" class="size_input" readonly name="descuento[]" value="${item.descuento}"></td>
        <td><input type="text" class="size_input" readonly name="subtotal[]" value="${item.total_format}"></td>
        <td><button type="button" class="btn btn-danger btn-sm delete_btn_prod_venta" name="${item.idart}"><i class="fas fa-trash-alt"></i></button></td>
        </tr>
        `; 
    }
    /**SCROLL QUE PERMITE PONER EL SCROLL A LA TABLA SI PASA LOS 200PX */
    var divscroll = document.querySelector(".tableFixHead");
    divscroll.style.height="268px";
    /**SE ENVIA EL TOTAL DE LA VENTA A LOS INPUTS CORESPONDIENTES*/
    const inputventotal =  document.querySelector("#inputventatotal");
    inputventotal.value = data.totales.total;
    //inputventotal.style.fontSize="15px";

    const inputtotalv = document.querySelector("#venttotal_venta");
    inputtotalv.value = data.totales.total;
    //inputtotalv.style.fontSize="15px";
    /**FUNCTION THAT DELETE PRODUCT SALE*/
    const btnventadelete =  document.querySelectorAll(".delete_btn_prod_venta");
    btnventadelete.forEach(btn=>{
        btn.addEventListener("click", (e) =>{
            e.preventDefault();
            const id = btn.getAttribute('name');
            delete_venta_product_temp(id);
        });
    });

}
/**FUNCTION QUE ME PERMITE ELEMINAR UN PRODUCTO DE LA TABLA DE LOS PRODUCTOS */
const delete_venta_product_temp = (id) => {
    //Token de seguridad
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr("content");
    var id_user = document.getElementById("id_user").value;
    var url = "/deleteventaproducto";
    var datos = new FormData();
    datos.append('id_user',id_user);
    datos.append('idprod', id);
    fetch(url, {
        headers: {
        'X-CSRF-TOKEN': CSRF_TOKEN// <--- aquí el token de seguridad.
        },
        method:'post',
        body:datos
    })
    .then(data => data.json())
    .then(data => {
        console.log('Success:', data);
        if (data.estado == 1 ) {
            pintar_tabla_venta (data);
        }else if(data.estado == 0){
            mensaje_error_save_venta_temp(data);
        }else if(data.estado == "sinproductos"){
            const sin_producto = document.querySelector("#tabla_venta_productos_temp");
            sin_producto.innerHTML = ""
        }
    })
    .catch(function(error){
        console.error('Error:', error)
    });

}
/**FUNCTION QUE ME PERMITE REALIZAR EL CALCULO DEL CAMBIO DE LA CANTIDAD QUE SE ENTRGO*/
const pcantidadv = document.querySelector("#ventdinero");
pcantidadv.addEventListener("keyup", event =>{
    //console.log("exit");
    //pcantidadv.style.fontSize="23px";
    const  vcambio = document.querySelector("#ventsuelto");
    const  ventventa = document.querySelector("#venttotal_venta");
    const vcantidad = Number(pcantidadv.value);
    const totalreplace = ventventa.value.replace(/\s+/g, '');
    const vtotal = Number.parseFloat(totalreplace);
    const tt = ventventa.value;
    console.log(tt)
    if ( tt === '0.00') {
        //console.log("no se genero con exito");
        return false;
    }
    //console.log("se formateo "+tt);
    console.log('cantidad :'+ vcantidad+ 'total : '+vtotal);
    if(vcantidad < vtotal){
        console.log("la cantidad es menor");
        vcambio.value="";
    }else if(vcantidad >= vtotal){
        const ventacambio = vcantidad - vtotal;
        //vcambio.style.fontSize="23px";
        let mycamb = truncateDecimals(ventacambio,2);
        //vcambio.value=ventacambio;
        //console.log("EL CAMBIO :"+ ventacambio);
        vcambio.value = mycamb;
        console.log("EL CAMBIO :"+ mycamb);
    }
});
/**fUNCION QUE PARSEA EL CAMBIO A 2 DECIMALES */
function truncateDecimals (num, digits) 
{
       var numS = num.toString();
       var decPos = numS.indexOf('.');
       var substrLength = decPos == -1 ? numS.length : 1 + decPos + digits;
       var trimmedResult = numS.substr(0, substrLength);
       var finalResult = isNaN(trimmedResult) ? 0 : trimmedResult;

       // adds leading zeros to the right
       if (decPos != -1){
           var s = trimmedResult+"";
           decPos = s.indexOf('.');
           var decLength = s.length - decPos;

               while (decLength <= digits){
                   s = s + "0";
                   decPos = s.indexOf('.');
                   decLength = s.length - decPos;
                   substrLength = decPos == -1 ? s.length : 1 + decPos + digits;
               };
           finalResult = s;
       }
       return finalResult;
};

/**FUNCTION THAT SAVE THE SALE*/
const save_guardar_venta = document.querySelector("#save_venta_total");
save_guardar_venta.addEventListener('submit', (e) =>{
    e.preventDefault();
    const btnsaveprodventa = document.querySelector("#venta_productos_realizada");
    btnsaveprodventa.innerHTML = "";
    btnsaveprodventa.disabled = true;
    btnsaveprodventa.innerHTML += `<span class="spinner-border spinner-border-sm" 
    role="status" aria-hidden="true"></span> Enviando datos...`;
    //alert("se ejecuto");
    const formdatosventa = new FormData(save_guardar_venta);
    console.log(formdatosventa);
    var url ="/saveformventa";
    fetch(url,{
        method:'post',
        body:formdatosventa
    })
    .then(data => data.json())
    .then(data =>{
        //console.log("Success:", data);
        if(data.estado == 1){
            //toastr.success(data.mensaje);
            save_guardar_venta.reset();
            document.querySelector("#ventfonio").value = data.folio;
            btnsaveprodventa.disabled = false;
            btnsaveprodventa.innerHTML = "Aceptar";
            const tablaventasindatos = document.querySelector("#tabla_venta_productos_temp");
            tablaventasindatos.innerHTML = ""
            /*Swal.fire(
                'El cambio es de : $ '+data.suelto,
                data.mensaje,
                'success'
            )*/
            print_ticket(data);
            var myModal = new bootstrap.Modal(document.getElementById('printModal'));
            myModal.show();
            
        }else if(data.estado == 0){
            mensaje_error_save_venta_temp(data);            
            btnsaveprodventa.disabled = false;
            btnsaveprodventa.innerHTML = "Aceptar";

        }else if (data.estado == "errorvalidacion") {
            mensaje_error_save_venta_temp(data); 
            btnsaveprodventa.disabled = false;
            btnsaveprodventa.innerHTML = "Aceptar";

        }
    })
    .catch(function(error){
        console.error("Error:", error);
            btnsaveprodventa.disabled = false;
            btnsaveprodventa.innerHTML = "Aceptar";

    });

   // Display the values
    /*for (var value of formdatosventa.values()) {
       console.log(value); 
    }*/
});
/**FUNCTION THAT PRINT THE TICKET*/
const print_ticket = (data) => {
    /**the add properties the modal messsage cambio and success sale*/
    document.querySelector("#messsageModalLabelSuccess").textContent = data.mensaje;
    document.querySelector("#cambio_sale").textContent = data.suelto;
    /**The add properties the ticket for print*/
    let detail =  data.sale_now.detail;
    //console.log(detail);
    const bodytabledetails = document.querySelector("#tbody_details");
    bodytabledetails.innerHTML = "";
    const template_details = document.querySelector("#template_details").content;
    const fragmetdetail = document.createDocumentFragment();
    document.querySelector("#sale_cliente").textContent = data.sale_now.sale.nombre;
    document.querySelector("#sale_date").textContent = data.sale_now.sale.fecha_hora;        
    document.querySelector("#sale_folio").textContent = data.sale_now.sale.num_folio;        
    document.querySelector("#sale_total").textContent = data.sale_now.sale.total_venta;        
    document.querySelector("#sale_cambio").textContent = data.suelto;        
    detail.forEach(element => {
        console.log(element);
        template_details.querySelector(".detail_cantidd").textContent=element.cantidad;
        template_details.querySelector(".detail_nombre").textContent=element.articulo;
        template_details.querySelector(".detail_venta").textContent=element.precio_venta;
        template_details.querySelector(".detail_subtotal").textContent=element.subtotal;
        const clonedetail = template_details.cloneNode(true);
        fragmetdetail.appendChild(clonedetail);

    });
    bodytabledetails.appendChild(fragmetdetail);
}

/**FUNCTION THAT PRINT THE TICKET FROM SALE*/
const now_print = document.querySelector("#print_now_ticket");
now_print.addEventListener("click", (e) =>{
    e.preventDefault();
    window.print();
});


/**FUNCTION QUE PERMITE CANCELAR LA VENTA POR COMPLETO*/
const cancelventaprod = document.querySelector("#cancelventaproducto");
cancelventaprod.addEventListener('click', (e) =>{ 
    cancelventaprod.innerHTML = "";
    cancelventaprod.disabled = true;
    cancelventaprod.innerHTML += `<span class="spinner-border spinner-border-sm" 
    role="status" aria-hidden="true"></span> Enviando datos...`;
     //Token de seguridad
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr("content");
    var id_user_eliminar = document.getElementById("id_user").value;
    var url = "/deleteventa";
    var datoselim = new FormData();
    datoselim.append('id_user',id_user_eliminar);
    fetch(url,{
        headers: {
        'X-CSRF-TOKEN': CSRF_TOKEN// <--- aquí el token de seguridad.
        },
        method:'post',
        body:datoselim
    })
    .then(data=> data.json())
    .then(data =>{
        console.log('success',data);
        if(data.estado == 1){
            toastr.success(data.mensaje); 
            /**se reinicia el formulario de la venta mediante el id del form*/
            save_guardar_venta.reset();
            /**aqui termina */
            cancelventaprod.disabled = false;
            cancelventaprod.innerHTML = "Aceptar";
            const limpiar_tabla = document.querySelector("#tabla_venta_productos_temp");
            limpiar_tabla.innerHTML = ""
        }else if (data.estado == 0) {
            mensaje_error_save_venta_temp(data);
            cancelventaprod.disabled = false;
            cancelventaprod.innerHTML = "Aceptar";   
        }
    })
    .catch(function(error){
        console.error('error',error);
        cancelventaprod.disabled = false;
        cancelventaprod.innerHTML = "Aceptar";
    });
   
});

