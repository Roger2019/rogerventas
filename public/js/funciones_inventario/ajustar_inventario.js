
class Ajustar{
    constructor(ruta){
        this.ruta = ruta;
    }

    errorgeneral(error_stock){
        let mserror = document.querySelector("#errors");
        mserror.innerHTML = `
            <div class="alert alert-danger text-center" role="alert">
                <strong>${error_stock}</strong>
            </div>
        `;
        setTimeout(() => {
            mserror.innerHTML="";
        }, 3000);
        return false;
        
    }
}

class Producto extends Ajustar{

    updateproducto = (dataform,CSRF_TOKEN,arraynewstock) =>{
        let input_vacio = 0;
        arraynewstock.forEach(element => {
            if (element == "") {
                input_vacio++;
            }
        });
        //console.log("campos vacios :"+input_vacio);
        let mserror = document.querySelector("#errors");
        if (input_vacio != 0 ) {
            mserror.innerHTML = `
                <div class="alert alert-danger text-center" role="alert">
                    <strong>Error !: Hay campos vacios en la columna => nuevo stock.</strong>
                </div>
            `;
            setTimeout(() => {
                mserror.innerHTML="";
            }, 3000);
            return false;
        }

        fetch(this.ruta, {
             headers:{
                'X-CSRF-TOKEN': CSRF_TOKEN// <--- aquí el token de seguridad.
            },
            method: 'POST',
            body:dataform
           
        }).then(data => data.json())
        .then(data=> {
            if (data.estado == 1) {
                Swal.fire(
                    'Exito!',
                    data.mensaje,
                    'success'
                )
            }else if(data.estado == 2){
                Swal.fire(
                    'Error!',
                    data.mensaje,
                    'error'
                );
            }
            console.log('Success:', data);
        })
        .catch(error => console.error('Error:', error));
    }

    
    startloadicon(button){
        button.innerHTML = "";
        button.disabled = true;
        button.innerHTML += `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Actualizando ...`;
    }

    endloadicon(endbutton,message_end){
        endbutton.disabled = false;
        endbutton.innerHTML = message_end;
        
    }

}
/**FUNCTION THAT UPDATE STOCK PRODUCTS*/
let CSRF_TOKEN = $('meta[name="csrf-token"]').attr("content");
const btnupdate = document.querySelector("#updatenewstock");
btnupdate.addEventListener("click", (e)=>{
    e.preventDefault();
    const data_art = document.querySelectorAll(".stock_actual");
    //console.log(data_art);
    let cont_stock = 0;
    data_art.forEach(element => {
        cont_stock++;
    });
    
    console.log(cont_stock);
    if (cont_stock == 0) {
        const errorstock = new Ajustar();
        errorstock.errorgeneral('Error !: No hay articulos para ajustar el inventario');
        return false;
    }

    const startload = new Producto(); 
    startload.startloadicon(btnupdate);

    setTimeout(() => {
        const formstock = document.querySelector("#updateproductstock");
        const dataform = new FormData(formstock);
        const arraynewstock = dataform.getAll('newstock[]');
        //console.log("vacio : "+arraynewstock);
        let ruta = "/updatearraystock/";
        const sendproducts = new Producto(ruta);
        sendproducts.updateproducto(dataform,CSRF_TOKEN,arraynewstock);

        let message_end = "Actualizar";
        const loadend = new Producto();
        loadend.endloadicon(btnupdate,message_end);
    }, 3000);
});