
class Inventario{
    constructor(ruta,id){
        this.ruta = ruta;
        this.id = id;
    }
}

class Getdatos extends Inventario{
    showproductos = async () =>{
        //console.log(this.ruta + this.id)
        try {
            let response = await fetch(this.ruta+this.id);
            let json = await response.json();
            console.log(json);
            let art = json.articles;
            const tablebodyshow = document.querySelector("#idbodyart");
            tablebodyshow.innerHTML ="";
            const templetearticle = document.querySelector("#template_show_articles").content;
            const fragmentart = document.createDocumentFragment();
            let i = 0;
            for (let item of art) {
                i++;
                templetearticle.querySelector(".art-consecutivo").textContent = i;
                templetearticle.querySelector(".art-codigo").textContent = item.codigo;
                templetearticle.querySelector(".art-name").textContent = item.nombre;
                templetearticle.querySelector(".art-id").value = item.idarticulo;
                templetearticle.querySelector(".art-stock").textContent = item.stock;
                const clonearticle = templetearticle.cloneNode(true);
                fragmentart.appendChild(clonearticle);
            }
            tablebodyshow.appendChild(fragmentart);
            document.querySelector("#art-reg").value = json.registros;
        } catch (error) {
            console.log("Ocurrio un error", error);
        }
    }

    sendarraydata = (arraycant,arrayart) =>{
        try{
            let input_vacio = 0;
            arraycant.forEach(element => {
                if (element == "") {
                    input_vacio++;
                }
                //console.log(element);
            });
            console.log("campos vacios :"+input_vacio);
            const ms_error = document.querySelector("#error-ms");
            if (input_vacio != 0) {
                ms_error.innerHTML="";
                ms_error.innerHTML+=`
                    <div class="alert alert-danger text-center" role="alert">
                        <strong>Error !: Hay campos vacios en la colunma cantidad.</strong>
                    </div>
                `;
                setTimeout(() => {
                    ms_error.innerHTML="";
                }, 3000);
                return false;
            }
            
            const total_reg = document.querySelector("#art-reg").value;
            let inc = 0;
            arraycant.forEach((element,index) => {
                inc++
                let CSRF_TOKEN = $('meta[name="csrf-token"]').attr("content");
                let art = arrayart[index];
                const formsend = new FormData();
                formsend.append('cant', element);
                formsend.append('art', art);
                (async () => {
                    let response = await fetch(this.ruta,{
                        headers: {
                            'X-CSRF-TOKEN': CSRF_TOKEN// <--- aquí el token de seguridad.
                        },
                        method: 'POST',
                        body: formsend
                    });
                    let json = await response.json();
                    console.log(json);
                })();
            });
            if (inc != 0) {
                let showbarr = document.querySelector("#progressbar");
                showbarr.innerHTML =`
                    <div class="progress" style="height: 25px;">
                        <div class="progress-bar progress-bar-striped bg-success" id="idprogessbar" role="progressbar" style="" aria-valuenow="" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                `;
                let barr = document.querySelector("#idprogessbar");
                let porcentaje = (inc * 100) / total_reg;
                console.log(inc+" "+"porcentaje :"+porcentaje)
                barr.style.width =porcentaje+"%";
                barr.innerHTML = porcentaje+"%";
                setTimeout(() => {
                    showbarr.innerHTML = "";
                    document .querySelector("#idbodyart").innerHTML ="";
                    barr.style.width = "0%";
                }, 3000);
            }else{
                ms_error.innerHTML="";
                ms_error.innerHTML+=`
                    <div class="alert alert-danger text-center" role="alert">
                        <strong>Error !: Esta categoria seleccionda no contiene productos.</strong>
                    </div>
                `;
                setTimeout(() => {
                    ms_error.innerHTML="";
                }, 3000);
                return false;
            }
            
        }catch(error) {
            console.log(error);
        }
    }
}

/**SEARCH CATEGORY OF PRODUCTS*/
const select_categoria = document.querySelector("#select_categoria");
select_categoria.addEventListener("change", (e) =>{
    let id = select_categoria.value
    let ruta = "/getarticulos/";
    const getinv = new Getdatos(ruta,id);
    getinv.showproductos();
});

/**SAVE THE  AMOUNT PRODUCT*/
const formcantidad = document.querySelector("#form-add-cantidad");
const btnaddcant = document.querySelector("#savecantidad");
btnaddcant.addEventListener("click", (e) =>{
    e.preventDefault();
    const formdata = new FormData(formcantidad);
    //console.log(formdata.getAll('insertcantprod[]'))
    //console.log(formdata.getAll('art[]'))
    const arraycant = formdata.getAll('insertcantprod[]');
    const arrayart = formdata.getAll('art[]');
    //console.log(arraycant.length);
    let ruta = "/saveinvdatos/";
    const send = new Getdatos(ruta);
    send.sendarraydata(arraycant,arrayart);
    /*for (let index = 0; index < arraycant.length; index++) {
        const element = arraycant[index];
        console.log(element);
    }*/
});

//https://www.youtube.com/watch?v=9Yi4O4G9LQs
//https://www.youtube.com/results?search_query=sistema+de+ajuste+de+de+inventario+
