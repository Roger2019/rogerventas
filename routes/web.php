<?php

use Illuminate\Support\Facades\Route;

use App\User;
use App\Permission\Models\Role;
use App\Permission\Models\Permission;
//use Illuminate\Support\Facades\Gate;
/**use controller version 8 */
use App\Http\Controllers\Caja\CajainicioController;
use App\Http\Controllers\Caja\CortecajaController;
use App\Http\Controllers\Caja\CorteparcialController;
use App\Http\Controllers\Inventario\InventarioController;
use App\Http\Controllers\Inventario\AjustarinventarioController;
use App\Http\Controllers\Admin\UserdashboardController;
use App\Http\Controllers\Cliente\ClienteController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('welcome');
    return view('connect.login');
});

/**Ruta principal */
Route::get('dashboard','Admin\AdminController@index')->name('admin')->middleware(['auth','Isadmin']);
Route::get('userdashboard', [UserdashboardController::class, 'index'])->name('userdashboard')->middleware('auth');
Auth::routes();

/**RUTAS DE AUTENTICACION DEL LOGIN*/
Route::get('/login', 'ConnectController@index')->name('login');
Route::post('/login','ConnectController@postLogin')->name('login');
/**RUTA DE CERRAR CESION */
Route::get('/logout', 'ConnectController@getLogout')->name('logout')->middleware('auth');


//Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

/**RUTAS DEL USUARIO PRINCIPAL*/
Route::resource('admin/role', 'RoleController')->names('role')->middleware('auth');
Route::resource('admin/user', 'UserController')->names('user')->middleware('auth');
Route::post('newuser', 'UserController@postRegister')->name('register')->middleware('auth');
Route::post('updatepasssword', 'UserController@updatepassword')->name('updatepasssword')->middleware('auth');
Route::get('delete-users/{id}', 'UserController@delete_user')->name('delete-users')->middleware('auth');
//Route::post('savedevolucionproduct', 'Devolucion\DevolucionventaController@store')->name('savedevolucionproduct');

/**RUTAS DE CATEGORIA */
Route::get('almacen/categoria', 'Categoria\CategoriaController@index')->name('categoria')->middleware('auth');
Route::post('/savecategoria', 'Categoria\CategoriaController@store')->name('savecategoria')->middleware('auth');
Route::get('showcategoria', 'Categoria\CategoriaController@show')->name('showcategoria')->middleware('auth');
Route::get('deletecategoria/{id}', 'Categoria\CategoriaController@destroy')->name('deletecategoria')->middleware('auth');
Route::get('categoria-list/{id}', 'Categoria\CategoriaController@edit')->name('categoria-list')->middleware('auth');
Route::post('categoriaupdate', 'Categoria\CategoriaController@update')->name('categoriaupdate')->middleware('auth');
//Route::get('product-list', 'Categoria\CategoriaController@show');

/**RUTAS PARA EL ARTICULO*/ 
Route::get('almacen/articulo', 'Articulo\ArticuloController@index')->name('articulo')->middleware('auth');
Route::post('savearticulo', 'Articulo\ArticuloController@store')->name('savearticulo')->middleware('auth');
Route::get('showproducto', 'Articulo\ArticuloController@show')->name('showproducto')->middleware('auth');
Route::get('product-list/{id}', 'Articulo\ArticuloController@edit')->name('product-list')->middleware('auth');
Route::post('updateproduct', 'Articulo\ArticuloController@update')->name('updateproduct')->middleware('auth');
Route::get('delete-category/{id}', 'Articulo\ArticuloController@destroy')->name('delete-category')->middleware('auth');

/**RUTAS PARA EL PROVEEDOR*/
Route::get('compras/proveedor', 'Proveedor\ProveedorController@index')->name('proveedor')->middleware('auth');
Route::post('saveproveedor', 'Proveedor\ProveedorController@store')->name('saveproveedor')->middleware('auth');
Route::get('showproveedor', 'Proveedor\ProveedorController@show')->name('showproveedor')->middleware('auth');
Route::get('provider-list/{id}', 'Proveedor\ProveedorController@edit')->name('provider-list')->middleware('auth');
Route::post('updateprovider', 'Proveedor\ProveedorController@update')->name('updateprovider')->middleware('auth');
Route::get('delete-provider/{id}', 'Proveedor\ProveedorController@destroy')->name('delete-provider')->middleware('auth');
/**RUTAS DE LOS INGRESOS COMPRAS DE LOS PRODUCTOS */
Route::resource('compras/entradas', 'Ingreso\IngresoController')->names('entradas')->middleware('auth');
Route::get('nombrearticuloentrada', 'Ingreso\IngresoController@find_nombre')->name('nombrearticuloentrada')->middleware('auth');
Route::post('temp_datos', 'Ingreso\IngresoController@save_temp')->name('temp_datos')->middleware('auth');
Route::post('showproveedores', 'Ingreso\IngresoController@searh_proveedores')->name('showproveedores')->middleware('auth');
// Route::post('', 'Ingreso\IngresoController@')->name('deleteproduct');
Route::post('deleteproduct', 'Ingreso\IngresoController@delete_prod')->name('deleteproduct')->middleware('auth');
Route::post('showproductostemp', 'Ingreso\IngresoController@show_prod')->name('showproductostemp')->middleware('auth');
Route::post('saveproductoentrada', 'Ingreso\IngresoController@store')->name('saveproductoentrada')->middleware('auth');
Route::get('showlistentradas', 'Ingreso\IngresoController@show')->name('showlistentradas')->middleware('auth');

Route::get('get-entrada/{id}', 'Ingreso\IngresoController@get_products')->name('get-entrada')->middleware('auth');

// Route::get('registro', 'Ingreso\IngresoController@create')->name('entradasproductos');

/**RUTAS DE LOS CLIENTES*/
Route::get('ventas/cliente', 'Cliente\ClienteController@index')->name('cliente')->middleware('auth');
Route::post('savecliente', 'Cliente\ClienteController@store')->name('savecliente')->middleware('auth');
Route::get('showlistcustomers', [ClienteController::class, 'show'])->name('showlistcustomers')->middleware('auth');
Route::get('get-data-cliente/{id}',[ClienteController::class, 'get_cliente'])->name('get-data-cliente')->middleware('auth'); 
Route::post('updatecliente', 'Cliente\ClienteController@update')->name('updatecliente')->middleware('auth');
Route::get('down-cliente/{id}',[ClienteController::class, 'down_cliente'])->name('down-cliente')->middleware('auth'); 

/**RUTAS DEL MODULO DE VENTAS*/
Route::resource('ventas/venta', 'Venta\VentaController')->names('venta')->middleware('auth');
Route::post('findproducto', 'Venta\VentaController@find_product')->name('findproducto')->middleware('auth');
Route::post('saveprodtempvent', 'Venta\VentaController@save_product_temp')->name('saveprodventa')->middleware('auth');
Route::post('showproductosventatemp', 'Venta\VentaController@show_vent_prod_tmp')->name('showproductosventatemp')->middleware('auth');
Route::post('deleteventaproducto', 'Venta\VentaController@delete_venta_product')->name('deleteventaproduct')->middleware('auth');
Route::post('saveformventa', 'Venta\VentaController@store')->name('saveformventa')->middleware('auth');
Route::post('deleteventa', 'Venta\VentaController@delete_venta_general')->name('deleteventaproduct')->middleware('auth');
Route::get('showlistventas', 'Venta\VentaController@show')->name('showlistventas')->middleware('auth');
Route::get('venta-detalle/{id}', 'Venta\VentaController@show_edit')->name('venta-detalle')->middleware('auth');
//Route::get('ventas/venta', 'Venta\VentaController@index')->name('venta');

/**DEVOLUCIONES DE UNA VENTA*/
Route::resource('devoluciones/venta', 'Devolucion\DevolucionventaController')->names('devolucion')->middleware('auth');
Route::get('products_devolucion/{folio}', 'Devolucion\DevolucionventaController@show_devolucion_venta')->name('products_devolucion')->middleware('auth');
Route::post('savedevolucionproduct', 'Devolucion\DevolucionventaController@store')->name('savedevolucionproduct')->middleware('auth');

/**RUTAS DE CAJA INICIO*/
Route::get('caja/cajainicio', [CajainicioController::class, 'index'])->name('cajainicio')->middleware('auth');
Route::post('saveapertura', [CajainicioController::class, 'store'])->name('saveapertura')->middleware('auth');
/**RUTAS DE CORTE DE CAJA*/
Route::get('caja/corte', [CortecajaController::class, 'index'])->name('corte')->middleware('auth');
Route::post('savecortecaja', [CortecajaController::class, 'store'])->name('savecortecaja')->middleware('auth');
Route::get('showlistcortes', [CortecajaController::class, 'show'])->name('showlistcortes')->middleware('auth');
/**TICKET FOR THE CUTTING OF THE CASHIER'S DAY*/
Route::get('ticketcorte', [CortecajaController::class, 'ticket'])->name('ticketcorte')->middleware('auth');

Route::get('caja/corteparcial', [CorteparcialController::class, 'index'])->name('corteparcial')->middleware('auth');
Route::get('showlistcajeros', [CorteparcialController::class, 'show'])->name('showlistcajeros')->middleware('auth');
Route::post('saveformparcial', [CorteparcialController::class, 'store'])->name('saveformparcial')->middleware('auth');

/**ROUTE FOR  INVENTORY*/
Route::get('inventario', [InventarioController::class, 'index'])->name('inventario')->middleware('auth');
Route::get('getarticulos/{id}', [InventarioController::class, 'get_articles'])->name('getarticulos')->middleware('auth');
Route::post('saveinvdatos', [InventarioController::class, 'addinventario'])->name('saveinvdatos')->middleware('auth');
Route::get('ajustarinventario', [AjustarinventarioController::class, 'index'])->name('ajustarinventario')->middleware('auth');
Route::post('updatearraystock', [AjustarinventarioController::class, 'updatenewstock'])->name('updatearraystock')->middleware('auth');
