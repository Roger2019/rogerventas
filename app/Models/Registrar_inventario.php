<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Registrar_inventario extends Model
{
    use HasFactory;

    protected $table='inventario';

    protected $primaryKey="idinventario";

    public $timestamps=false;

    protected $fillable = [
        'user_id',
        'fecha',
        'hora',
        'estatus',
    ];
}
