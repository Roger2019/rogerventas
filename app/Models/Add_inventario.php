<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Add_inventario extends Model
{
    use HasFactory;

    protected $table='capturarinventario';

    protected $primaryKey="idcaptura";

    public $timestamps=false;

    protected $fillable = [
        'articulo_id',
        'cantidad',
        'fecha',
        'hora',
    ];
}
