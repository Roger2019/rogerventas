<?php

namespace App\Http\Controllers\devolucion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Models\Venta_producto;
use App\Models\Detalle_venta_producto;
use App\Models\Devolucion_producto;
use App\Models\Detalle_devolucion_producto;

use Carbon\Carbon;
use DB;
use Response,Validator;

class DevolucionVentaController extends Controller
{
    public function index()
    {
        Gate::authorize('haveaccess','devolucion_producto.index');
        return view("devolucion.devolucion_venta.index");
    }

    public function show_devolucion_venta($folio)
    {
        try {
            $getventadetails=DB::table('ventas as v')
            ->join('clientes as c', 'v.cliente_id','=','c.idcliente')
            ->join('detalle_ventas as dv','v.idventa','=','dv.venta_id')
            ->select('v.idventa','v.fecha_hora','c.nombre','v.tipo_comprobante','v.num_folio','v.estado','v.total_venta')
            ->where('v.num_folio','=',$folio)
            ->first();
            $id = $getventadetails->idventa;
            $detalles=DB::table('detalle_ventas as d')
            ->join('productos as a','d.articulo_id','=','a.idarticulo')
            ->select('a.idarticulo','a.nombre as articulo','d.cantidad','d.descuento','d.precio_venta','d.subtotal')
            ->where('d.venta_id','=',$id)
            ->get();

            /*$where = array('num_folio'=>$folio);
            $provider = Venta_producto::where($where)->first();*/
            return Response::json([
                'getventa'=>$getventadetails,
                'detalles'=>$detalles
            ]);

        } catch (\Throwable $th) {
            $m = 'Excepción capturada: '.$th->getMessage(). "\n";
            return response()->json([
                'estado'=>0,
                'mensaje'=>'Error. No se encontro ninguna venta con este folio',
                'class'=>'danger',
                'error' => (array) $m
            ]);  
        }

         
    }

    public function store(Request $request){
        try {
           
            /*if (is_array($request->descripcion)) {
                 return response()->json([
                    'estado' => 'errorvalidacion',
                    'mensaje'=> 'no se tienen datos en el array de descripcion',
                    'uploaded_image' => '',
                    'class_name' => 'alert-danger'
                ]);
            }*/
           
            //START THE TANSACTION
            DB::beginTransaction();
            
            //THE RETURN DATA IS SAVED
            $dev = new Devolucion_producto();
            $dev->venta_id = $request->idventades;
            $dev->observacion = "Devolucion de productos";
            $hora = Carbon::now('America/Mexico_City');
            $dev->fecha=$hora->toDateTimeString();
            $dev->save();
            //VARIABLE THAT HAS THE CONSECUTIVE NUMBER OF PRODUCTS   
            $num = $request->numero;
            //THE VARIABLES ARE OBTAINED TO SAVE IN THE TABLE DETALLE_DEVOLCUCION_PRODUCTO    
            $nombre = $request->nombre;
            $cantidad = $request->cantidad;
            $pventa= $request->pventa;
            $descuento = $request->descue;
            $subtotal = $request->subtotal;
            $desc = $request->descripcion;
            //dd($desc);
            /*if (empty($desc)) {
                echo("The array is empty.");
            }*/
            //echo count($desc);

            $cont = 0;
            $devc = 0;
            while ($cont < count($num)) {
                $descript = $desc[$cont];
                //si la varible $descript en su actual valor no esta vacio entonces significa que se tiene que devolver. y entonces entra al if y se devuelve 
                if ($descript != "") {
                    $detalle = new  Detalle_devolucion_producto();
                    $detalle->devolucion_id = $dev->iddevolucion;
                    $detalle->articulo_id = $num[$cont];
                    $detalle->nombre = $nombre[$cont];      
                    $detalle->cantidad = $cantidad[$cont];
                    $detalle->pventa= $pventa[$cont];
                    $detalle->descuento= $descuento[$cont];
                    $detalle->subtotal = $subtotal[$cont];
                    $detalle->motivo = $descript;
                    $detalle->save();
                    $devc=$devc+1;
                    //echo $descript;
                }
                $cont=$cont+1;
            }
            
            DB::commit();
            /**IT IS VALID THAT SOME PRODUCT WILL BE RETURNED*/
            if ($devc == 0) {
                return Response::json([
                    "estado"=>"errorvalidacion",
                    "mensaje"=>"Error: No has seleccionado un producto para la devolucion",
                    "class"=>"danger",
                ]);
            }else{
                return Response::json([
                    "estado"=>1,
                    "mensaje"=>"Se realizo la devolucion con exito",
                    "class"=>"success",
                ]);    
            }
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            $m = 'Excepción capturada: '.$th->getMessage(). "\n";
            return response()->json([
                'estado'=>0,
                'mensaje'=>'Ocurrio un error, No se puede realizar la devolucion de los productos seleccionados',
                'class'=>'danger',
                'error' => (array) $m
            ]);      

        }
       
    }
}