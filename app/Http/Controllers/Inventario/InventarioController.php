<?php

namespace App\Http\Controllers\Inventario;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Http\Controllers\General\FechaController;
use App\Models\Add_inventario;

use Carbon\Carbon;
use Validator, Hash, Auth, Response;
use DataTables;
use DB;

class InventarioController extends Controller
{
    public function index()
    {
        Gate::authorize('haveaccess','inventario_cantidad.index');
        $categorias = DB::table('categorias')->get();
        return view('inventario.addinventario.index',['categorias'=>$categorias]);
    }

    public function get_articles($id)
    {
        $articles = DB::table('productos')->where([
            ['categoria_id','=',$id],
            ['estado','=','Activo'],
        ])
        ->get();
        $registros = count($articles);
        return response()->json([
            "articles"=>$articles,
            "registros"=>$registros
        ]);
    }

    public function addinventario(Request $request)
    {
        $date = new FechaController();
        $date_now = $date->datenow();
        $date_time = $date->timenow();
        /**SAVE THE CAPTURARINVENTARIO*/
        $addinv = new Add_inventario();
        $addinv->articulo_id = $request->art;
        $addinv->cantidad = $request->cant;
        $addinv->fecha = $date_now;
        $addinv->hora =  $date_time;

        if ($addinv->save()) {
            return response()->json([
                "estatus"=>$request->art
            ]);
        }else{
            return response()->json([
                "estatus"=>0
            ]);
        }
        //return response()->json([
            //"new"=>$request->art,
            //"id"=>$request->cant,
            //"fecha"=>$date_now,
            //"hora"=>$date_time
        //]);
    }
}
