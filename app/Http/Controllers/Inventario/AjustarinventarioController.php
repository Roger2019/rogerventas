<?php

namespace App\Http\Controllers\Inventario;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Http\Controllers\General\FechaController;
use App\Models\Articulo;
use App\Models\Registrar_inventario;
use Carbon\Carbon;
use Validator, Hash, Auth, Response;
use DataTables;
use DB;

class AjustarinventarioController extends Controller
{
    public function index()
    {
        Gate::authorize('haveaccess','inventario_ajustar.index');
        $date = new FechaController();
        $date_now = $date->datenow();
        $products = DB::table("capturarinventario as cap")
        ->join('productos as pro', 'cap.articulo_id', '=', 'pro.idarticulo')
        ->select('cap.idcaptura','pro.nombre','cap.cantidad')
        ->select('cap.idcaptura','cap.articulo_id','pro.nombre','pro.stock','cap.cantidad')
        ->where("cap.fecha","=","$date_now")
        ->orderBy('cap.idcaptura','asc')
        ->get();

        /*$products = DB::table("productos as pro")
        ->join('capturarinventario as cap', 'pro.idarticulo', '=', 'cap.articulo_id')
        ->select('cap.idcaptura','pro.nombre','cap.cantidad')
        ->orderBy('cap.idcaptura','asc')
        ->get();*/
        return view('inventario.ajustarinventario.index', ['products'=>$products]);
    }

    public function updatenewstock(Request $request)
    {
        try {

            DB::beginTransaction();
            $id  = Auth::id();
            $date = new FechaController();
            $date_now = $date->datenow();
            $date_time = $date->timenow();
            /**IT IS VALIDAD IF THERE IS ONE INVENTORY SAVE TODAY*/
            $getinv = DB::table('inventario')->where("fecha","=","$date_now")->get();
            $fecha_inventario = count($getinv);
            //$fecha_inventario = $getinv[0]->fecha;
            if ($fecha_inventario != 0) {
                return response()->json([
                    'estado'=>2,
                    'mensaje'=>'Error!: Ya se realizo el ajuste de inventario el dia de hoy'
                ]);
            }

            $inventario = new Registrar_inventario();
            $inventario->user_id = $id;
            $inventario->fecha = $date_now;
            $inventario->hora = $date_time;
            $inventario->estatus = "Activo";
            $inventario->save();
            /**update stock products*/
            $arrayarticulo = $request->art;
            $arraystock = $request->newstock;
            $art_total = count($arrayarticulo);
            $total = [];
            $acumulado = [];
            $increment = 0;
            $position = 0;
            foreach ($arraystock as $row) {
            $incr = $increment++;
            $index = $position++;
            $idart = $arrayarticulo[$index];
            $newstockpro = Articulo::find($idart);
            $newstockpro->stock=$row;
            if ($newstockpro->update()) {
                $total[]=['num'=>$increment];
            }
            //$acumulado[]=['siguiente'=>$idart];
                //echo "Valor actual de \$arraystock: $row.\n";
                //echo $row;
            }

            $percentage = round(($increment * 100) / $art_total);

            DB::commit();

            return response()->json([
                'estado'=>1,
                'mensaje'=>'Los productos se actualizaron con exito',
                //'today'=>$fecha_inventario
               /*'array'=>$arraystock,
                'total'=>$total,
                'registros'=>$increment,
                'art'=>$art_total,*/
                //'porcentaje'=>$percentage
                //'acululado'=>$acumulado
            ]);

        } catch (\Throwable $th) {
            DB::rollBack();
            $m = 'Excepción capturada: '.$th->getMessage(). "\n";
            return response()->json([
                "estado" => 0,
                "mensaje"=> (array) $m
            ]);
        }
    }
}
