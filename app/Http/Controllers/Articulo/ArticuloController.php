<?php

namespace App\Http\Controllers\Articulo;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Models\Articulo;
use DataTables;
use File;
use Response,Validator,Str,Config;
use DB;

class ArticuloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Gate::authorize('haveaccess','almacen_articulo.index');
        $categoria = DB::table('categorias')->where('estatus','=','1')->get();
        return view('almacen.articulo.index',['categoria'=>$categoria]);
    }

    public function store(Request $request)
    {   
        //https://www.youtube.com/watch?v=KkUG3nfcsIM
        $articulo = new Articulo;

        if ($request->ajax()) {

            if($files = $request->file('imagen') == null){
                $articulo->imagen='productogeneral.png';
                $rules = [
                    'nombre' => 'required',
                    'idcategoria' => 'required',
                    'codigo' => 'required|unique:productos,codigo',
                    'stock' => 'required',
                    'pcompra'=>'required',
                    'pventa'=>'required',
                    'descripcion' => 'required',
                    'iva' => 'required',
                    'articulo_des' => 'required'
                ];

                $messages = [
                    'nombre.required' => 'El nombre es requerido',
                    'idcategoria.required' => 'La categoria es requerida',
                    'codigo.required' => 'El codigo del producto es requerido',
                    'codigo.unique' => 'EL codigo para el producto ya existe, agrege otro codigo para elproducto',
                    'stock.required' => 'El stock del producto es requerido',
                    'pcompra.required' => 'El precio de compra es requerido',
                    'pventa.required' => 'El precio de venta es requerido',
                    'descripcion.required' => 'La descripcion es requerida',
                    'iva.required' => 'Es necesario saber si lleva iva o no',
                    'articulo_des.required' => 'Debes de poner el descuento, si este no lleva descuento solo agrega un 0' 
                ];
            }else{

                $rules = [
                    'nombre' => 'required',
                    'idcategoria' => 'required',
                    'codigo' => 'required',
                    'stock' => 'required',
                    'pcompra'=>'required',
                    'pventa'=>'required',
                    'imagen' => 'required|image|mimes:jpeg,png,jpg|max:2048',
                    'descripcion' => 'required',
                    'iva' => 'required',
                    'articulo_des' => 'required'
                ];

                $messages = [
                    'nombre.required' => 'El nombre es requerido',
                    'idcategoria.required' => 'La categoria es requerida',
                    'codigo.required' => 'El codigo del producto es requerido',
                    'stock.required' => 'El stock del producto es requerido',
                    'pcompra.required' => 'El precio de compra es requerido',
                    'pventa.required' => 'El precio de venta es requerido',
                    'descripcion.required' => 'La descripcion es requerida',
                    'imagen.required' => 'Debes de agregar una imagen',
                    'imagen.image' => 'Debe de ser una imagen del producto',
                    'imagen.mimes' => 'La imagen dede de contener una de la siguientes extensiones, jpeg,png,jpg',
                    'imagen.max' => 'La imagen debe de pesar menos de 2048',
                    'descripcion.required' => 'La descripcion es requerida',
                    'iva.required' => 'Es necesario saber si lleva iva o no',
                    'articulo_des.required' => 'Debes de poner el descuento, si este no lleva descuento solo agrega un 0' 
                ];
            }
            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()) {
                return response()->json([
                    'estado' => 'errorvalidacion',
                    'mensaje'=> $validator->errors()->all(),
                    'uploaded_image' => '',
                    'class_name' => 'alert-danger'
                ]);
            }

            ///////////////////////////

            // $articulo = new Articulo;
            $articulo->categoria_id=$request->get('idcategoria');
            $articulo->codigo=$request->get('codigo');
            $articulo->nombre=$request->get('nombre');
            $articulo->stock=$request->get('stock');
            $articulo->pcompra=$request->get('pcompra');
            $articulo->pventa=$request->get('pventa');
            $articulo->descripcion=$request->get('descripcion');
            $articulo->estado="Activo";
            $articulo->descuento=$request->get('articulo_des');
            $articulo->iva=$request->get('iva');
            if ($files = $request->file('imagen')) {
                $destinationPath = public_path('/imagenes/articulos');
                //$profileImagen = $files->getClientOriginalExtension();
                $profileImagen = trim($files->getClientOriginalName());
                $files->move($destinationPath, $profileImagen);
                $articulo->imagen=$profileImagen;
            }
            if($articulo->save()){
                return response()->json([
                    'estado'=> 1,  
                    'mensaje' => 'Se guardo el articulo con exito'
                ]);
            }else{
                return response()->json([
                    'estado'=> 0,
                    'mensaje' => 'Ocurrio un error, intentalo de nuevo'
                ]);
            }

        }
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
       
        $articulo = DB::table('productos')->where('estado','=','Activo')->get();

        return DataTables::of($articulo)
        ->addColumn('action', function($articulo){
            $id = $articulo->idarticulo;
            $button = '<button type="button" class="btn btn-secondary btn-sm btn-flat" onclick="edit_product('.$id.');" ><i class="far fa-edit"></i></button> &nbsp;&nbsp;&nbsp;';
            $button .= '<button type="button" class="btn btn-danger btn-sm btn-flat" onclick="delete_product('.$id.');"><i class="fas fa-trash-alt"></i></button>';
            return $button;
        })
        ->rawColumns(['action'])
        ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $where = array('idarticulo'=>$id);
        $producto = Articulo::where($where)->first();
        return Response::json($producto);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $articulo = Articulo::findOrFail($request->idprod);
            
            if($file = $request->file('upimagen') == null){
                $articulo->imagen='productogeneral.png';
                $rules = [
                    'upnombre' => 'required',
                    'upidcategoria' => 'required',
                    'upcodigo' => 'required',
                    'upstock' => 'required',
                    'uppcompra'=>'required',
                    'uppventa'=>'required',
                    'updescripcion' => 'required',
                    'upiva' => 'required',
                    'uparticulo_des' => 'required'
                ];        
                

                $messages = [
                    'upnombre.required' => 'El nombre es requerido',
                    'upidcategoria.required' => 'La categoria es requerida',
                    'upcodigo.required' => 'El codigo del producto es requerido',
                    'upstock.required' => 'El stock del producto es requerido',
                    'uppcompra.required' => 'El precio de compra es requerido',
                    'uppventa.required' => 'El precio de venta es requerido',
                    'updescripcion.required' => 'La descripcion es requerida',
                    'upiva.required' => 'Es necesario saber si lleva iva o no',
                    'uparticulo_des.required' => 'Debes de poner el descuento, si este no lleva descuento solo agrega un 0' 
                ];
                
            }else{
                $rules = [
                    'upnombre' => 'required',
                    'upidcategoria' => 'required',
                    'upcodigo' => 'required',
                    'upstock' => 'required',
                    'uppcompra'=>'required',
                    'uppventa'=>'required',
                    'upimagen' => 'required|image|mimes:jpeg,png,jpg|max:2048',
                    'updescripcion' => 'required',
                    'upiva' => 'required',
                    'uparticulo_des' => 'required'
                ];

                $messages = [
                    'upnombre.required' => 'El nombre es requerido',
                    'upidcategoria.required' => 'La categoria es requerida',
                    'upcodigo.required' => 'El codigo del producto es requerido',
                    'upstock.required' => 'El stock del producto es requerido',
                    'uppcompra.required' => 'El precio de compra es requerido',
                    'uppventa.required' => 'El precio de venta es requerido',
                    'updescripcion.required' => 'La descripcion es requerida',
                    'upimagen.required' => 'Debes de agregar una imagen',
                    'upimagen.image' => 'Debe de ser una imagen del producto',
                    'upimagen.mimes' => 'La imagen dede de contener una de la siguientes extensiones, jpeg,png,jpg',
                    'upimagen.max' => 'La imagen debe de pesar menos de 2048',
                    'updescripcion.required' => 'La descripcion es requerida',
                    'upiva.required' => 'Es necesario saber si lleva iva o no',
                    'uparticulo_des.required' => 'Debes de poner el descuento, si este no lleva descuento solo agrega un 0' 
                ];

            }

            $validator = Validator::make($request->all(), $rules, $messages);
                if ($validator->fails()) {
                    return response()->json([
                        'estado' => 'errorvalidacion',
                        'mensaje'=> $validator->errors()->all(),
                        'uploaded_image' => '',
                        'class_name' => 'alert-danger'
                    ]);
                }

            $articulo->categoria_id=$request->get('upidcategoria');
            $articulo->codigo=$request->get('upcodigo');
            $articulo->nombre=$request->get('upnombre');
            $articulo->stock=$request->get('upstock');
            $articulo->pcompra=$request->get('uppcompra');
            $articulo->pventa=$request->get('uppventa');
            $articulo->descripcion=$request->get('updescripcion');
            $articulo->estado="Activo";
            $articulo->descuento=$request->get('uparticulo_des');
            $articulo->iva=$request->get('upiva');
            if ($file = $request->file('upimagen')) {
                $destinationPath = public_path('/imagenes/articulos');
                $profileImagen = trim($file->getClientOriginalName());
                $file->move($destinationPath, $profileImagen);
                $articulo->imagen=$profileImagen;
            }
            if($articulo->update()){
                return response()->json([
                    'estado'=> 1,  
                    'mensaje' => 'Se actualizo correctamente el articulo'
                ]);
            }    
            
        
        } catch (\Throwable $th) {
            $m = 'Ocurrio un error: '.$th->getMessage(). "\n";
            return response()->json([
                    'estado'=> 0,
                    'mensaje' => (array) $m
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $articulo = Articulo::find($id);
        $articulo->estado="Inactivo";
        if ($articulo->update()) {
            return response()->json([
                "estado"=>1,
                "mensaje"=>"Se elimino correctamente el articulo"
            ]);
        }else{
            return response()->json([
                "estado"=>0,
                "mensaje"=>"Ocurrio un error.Intentalo de nuevo"
            ]);
        }
        
    }

}
