<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\General\FechaController;

use DB;

class AdminController extends Controller
{
    public function index(){

        $date = new FechaController();
        $date_now = $date->datenow();
        $ventas = DB::table('ventas')
        ->whereRaw("CAST(fecha_hora AS DATE) ='$date_now'")
        ->get();
        $total_ventas = count($ventas);
        $articulos= DB::table('productos')
        ->where('estado','=','Activo')
        ->get();
        $total_articulos = count($articulos);
        $entradas= DB::table('ingresos')
        ->whereRaw("CAST(fecha_hora AS DATE) ='$date_now'")
        ->get();
        $total_entradas = count($entradas);


        $getapertura = DB::table('aperturacajas')->where([
          ['estatus','=','Abierta'],
        ])
        ->whereRaw("CAST(fecha_hora AS DATE) ='$date_now'")
        ->get();
        $total_cajas = count($getapertura);

        return view('admin.admin.index',["ventas"=>$total_ventas, "productos"=>$total_articulos, "entradas"=>$total_entradas, "cajas"=>$total_cajas]);
    }
}
