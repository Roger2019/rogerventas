@extends('connect.master')
{{-- @section('title', 'Login') --}}
@section('content')
<div class="login-box bg-secondary">
  <!--div class="login-logo">
    <a href="../../index2.html">ROGER VENTAS</a>
  </div-->
  <div class=" row justify-content-center">
    <div class=" justify-content-center brand-link ">
        <img src="{{asset('dist/img/ICON_YOUTUBE.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
            style="opacity: .8">
        <span class=""><strong>OGER VENTAS</strong></span>
    </div>
  </div>

  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Inicia sesión co tu cuenta</p>
      <!--inicio del formulario-->
      {!! Form::open(['url'=>'/login'])!!}  
      {{-- <form action="../../index3.html" method="post"> --}}
        <div class="input-group mb-3">
          {{-- <input type="email" class="form-control" placeholder="Email"> --}}
          {!! Form::email('email',null,['class'=>'form-control','placeholder'=>'Correo']) !!}
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          {{-- <input type="password" class="form-control" placeholder="Password"> --}}
          {!! Form::password('password',['class'=>'form-control','placeholder'=>'password']) !!}
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          {{-- <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember">
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div> --}}
          <!-- /.col -->
          <div class="container">
            {{-- <button type="submit" class="btn btn-primary btn-block">Sign In</button> --}}
            {!! Form::submit('Ingresar',['class'=>'btn btn-info btn-block']) !!}
          </div>
          <!-- /.col -->
        </div>
      {{-- </form> --}}
      {!! Form::close()!!}
      @if(Session::has('message'))
      <div class="container"><br>
        <div class="alert alert-{{ Session::get('typealert') }}" style="display:none;">
        {{ Session::get('message') }}
          @if($errors->any())
            <ul>
              @foreach($errors->all() as $error )
                <li>{{$error}}</li>
              @endforeach
            </ul>
          @endif
          <script>
          //$(".alert").slideDown();
          setTimeout(function(){ $('.alert').slideDown(); }, 1000);
          setTimeout(function(){ $('.alert').slideUp(); }, 5000);

          </script>
        </div>
      </div>
      @endif
      <!--fin del formulario-->  
      {{-- <div class="social-auth-links text-center mb-3">
        <p>- OR -</p>
        <a href="#" class="btn btn-block btn-primary">
          <i class="fab fa-facebook mr-2"></i> Sign in using Facebook
        </a>
        <a href="#" class="btn btn-block btn-danger">
          <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
        </a>
      </div> --}}
      <!-- /.social-auth-links -->

      {{-- <p class="mb-1">
        <a href="forgot-password.html">I forgot my password</a>
      </p> --}}
      <br>
      <p class="mb-0">
        {{-- <a href="{{ url('/register') }}" class="text-center">Registrar un nuevo usuario</a> --}}
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->
@stop