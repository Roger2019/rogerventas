<section class="content margindivsection">
    <div class="d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
      <div class="d-flex align-items-center">
        <div>
          <a href="{{route('entradas.create')}}" class="btn btn-secondary btn-sm mr-3" id="btnshowmodalprovider">
            <i class="fa fa-archive"></i>
            <strong> Agregar nuevo proveedor</strong>
          </a>
        </div>
        <!--div>
          <button type="button" class="btn btn-info btn-sm mr-3">
            <i class="fa fa-archive"></i>
            <strong>Agregar nueva categoria</strong>
          </button>
        </div>
        <div>
          <button type="button" class="btn btn-info btn-sm mr-3">
            <i class="fa fa-archive"></i>
            <strong>Agregar nueva categoria</strong>
          </button>
        </div-->
      </div>
    </div>
</section>

<!-- MODAL PARA AGREGAR NUEVO PROVEEDOR -->
<div class="modal fade" id="ModalSaveProveedor" tabindex="-1" role="dialog" aria-labelledby="ModalSaveProveedorLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ModalSaveProveedorLabel">Agregar nuevo proveedor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            {!! Form::open(['id'=>'save_proveedor'])!!}
            <div class="form-group">
            <label>Nombre</label>
                <input type="text" class="form-control" name="nombre">
            </div>
             <div class="form-group">
            <label>Direccion</label>
                <input type="text" class="form-control" name="direccion">
            </div>
             <div class="form-group">
            <label>Telefono</label>
                <input type="text" class="form-control" name="telefono">
            </div>
            <div class="form-group">
            <label>Email</label>
                <input type="text" class="form-control" name="email">
            </div>
             @include('custom.validate_save_form_ajax')
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-primary" id="btnsaveproveedor">Guardar</button>
            {!!Form::close()!!}
      </div>
    </div>
  </div>
</div>

<!-- MODAL THAT UPDATE ONE PROVIDER-->
<div class="modal fade" id="ModalUpdateProvider" tabindex="-1" role="dialog" aria-labelledby="ModalSaveProveedorLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ModalSaveProveedorLabel">Agregar nuevo proveedor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            {!! Form::open(['id'=>'update_proveedor'])!!}
            <div class="form-group">
              <input type="text" name="upid" id="upid" hidden="true">
              <label>Nombre</label>
                <input type="text" class="form-control" id="upnombre" name="upnombre">
            </div>
             <div class="form-group">
            <label>Direccion</label>
                <input type="text" class="form-control" id="updireccion" name="updireccion">
            </div>
             <div class="form-group">
            <label>Telefono</label>
                <input type="text" class="form-control" id="uptelefono" name="uptelefono">
            </div>
            <div class="form-group">
            <label>Email</label>
                <input type="text" class="form-control" id="upemail" name="upemail">
            </div>
             @include('custom.validate_update_form_ajax')
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-primary">Actualizar</button>
            {!!Form::close()!!}
      </div>
    </div>
  </div>
</div>