<div id="ticket">
    <img
        src="{{asset('imagenes/articulos/productogeneral.png')}}"
        alt="Logotipo" id="logo_ticket">
    <p class="centrado" id="sale_cliente"></p>
    <p class="centrado" id="sale_date"></p>
    <table id="content_prod" style="">
        <thead>
            <tr class="tr_table">
                <th class="tick_cant">CANT</th>
                <th class="tick_art">ARTICULO</th>
                <th class="tick_precio">PRECIO</th>
                <th class="tick_total">TOTAL</th>
            </tr>
        </thead>
        <tbody id="tbody_details">
        </tbody>
        <tfoot>
             <tr class="tr_table">
                <td class="" colspan="3">VENTA TOTAL</td>
                <td class="" id="sale_total"></td>
            </tr>
            <tr class="tr_table">
                <td class="" colspan="3">CAMBIO</td>
                <td class="" id="sale_cambio"></td>
            </tr>
        </tfoot>
    </table>
    <div class="centrado">
        <p>FOLIO : <span id="sale_folio"></span></p>
    </div>
    <p class="centrado">¡GRACIAS POR SU COMPRA!<br>compania s.a de de c.v</p>
</div>


<!-- Modal success and ticket-->
<div class="modal fade" id="printModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-content-cambio">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center" id="messsageModalLabelSuccess"></h5>
        <!--button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button-->
      </div>
      <div class="modal-body">
        <div class="mb-3 text-center">
          <h5><strong> EL CAMBIO ES DE $ : <span id="cambio_sale"></span></strong></h5>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="print_now_ticket">Imprimir ticket</button>
      </div>
    </div>
  </div>
</div>

<template id="template_details">
    <tr class="tr_table">
        <td class="tick_cant detail_cantidd"></td>
        <td class="tick_art detail_nombre"></td>
        <td class="tick_precio detail_venta"></td>
        <td class="tick_total detail_subtotal"></td>
    </tr>
</template>

