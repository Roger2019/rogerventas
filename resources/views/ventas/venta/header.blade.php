<section class="content margindivsection">
    <!--div class="row">
        <div class="col-md-12">
            <div class="card card-outline card-info styledivproducto">
                <div class="row">
                    <div class="col-3 col-md-3">
                        <a href="{{route('venta.create')}}"><button class="btn btn-primary">Realizar nueva venta</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div-->
	<div class="d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center">
      <div>
	      <a href="{{route('venta.create')}}" class="btn btn-secondary btn-sm mr-3">
          <i class="fa fa-archive"></i>
          <strong> Realizar una venta</strong>
        </a>
      </div>
			<!--div>
				<button type="button" class="btn btn-info btn-sm mr-3">
          <i class="fa fa-archive"></i>
          <strong>Agregar nueva categoria</strong>
        </button>
			</div>
			<div>
				<button type="button" class="btn btn-info btn-sm mr-3">
          <i class="fa fa-archive"></i>
          <strong>Agregar nueva categoria</strong>
        </button>
			</div-->
		</div>
	</div>
</section>