@extends('layouts.admin')
@section('contenido')
<!--@include('devolucion.devolucion_venta.header')-->
<section class="margindivsection">
    <div class="card">
        <div class="card-header border">
            <h4>Devolucion de la venta</h4>
        </div>
        <div class="card-body border">
            <div id="messagedevoluciones"></div>
            <div class="row g-3 align-items-center">
                <div class="col-auto">
                    <label for="" class="col-form-label">Folio</label>
                </div>
                <div class="col-auto">
                    <input type="text" id="foliosaledev"  class="form-control" placeholder="ingresa el folio de la venta">
                </div>
                <div class="col-auto">
                    <span id="" class="form-button">
                    <button class="btn btn-secondary" id="searchsaledev"><i class="fas fa-search"></i> Buscar</button>
                    </span>
                </div>
            </div><br>
            {!!Form::open(['id'=>'save_producto_devolucion'])!!}
            <div class="row">
                
                <input type="number" name="idventades" id="idventades" hidden>
                <div class="table-responsive col-md-12" style="">
                    <table id="" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                            <th hidden>#</th>
                            <th >Articulo</th>
                            <th style="width:10%">Cantidad</th>
                            <th style="width:10%">P venta</th>
                            <th style="width:10%">Descuento</th>
                            <th style="width:10%">Subtotal</th>
                            <th >Devolucion</th>
                            </tr>
                        </thead>
                        <tbody id="tbodydevoluciones">
                           
                        </tbody>
                    </table>
                </div>
            </div><br>
            <div class="row">
                <div class="col-md-12">
                  <button class="btn btn-info" id="save_devolucion" >Realizar devolución</button>
                </div>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</section>
@push('ScriptDevolucionVenta')
<script src="{{asset('js/funciones_devolucion/devolucion_venta.js')}}"></script> 
@endpush
@endsection