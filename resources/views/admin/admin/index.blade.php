@extends ('layouts.admin')
@section ('contenido')
    <!--div class="row" style="margin-top: 11px;">
        <div class="col-12 col-sm-6 col-xxl d-flex">
            <div class="card illustation flex-fill">
                <div class="card-body p-0 d-flex flex-fill">
                    <div class="row no-gutters w-100">
                        <div class="col-6">
                            <div class="illustration-text p-3 m-l">
                                <h4 class="illustration-text">Total de ventas del dia de hoy</h4>
                                <p class="bm-0">my new dashboard</p>
                            </div>
                        </div>
                        <div class="col-6 align-self-start p-3 text-right">
                            <p>ventas del dia. total de articulos. usuarios. stock minimo. productos mas vendidos</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-xxl d-flex">
            <div class="card illustation flex-fill">
                <div class="card-body p-0 d-flex flex-fill">
                    <div class="row no-gutters w-100">
                         <div class="col-6">
                            <div class="illustration-text p-3 m-l">
                                <h4 class="illustration-text">welcome to page</h4>
                                <p class="bm-0">my new dashboard</p>
                            </div>
                        </div>
                        <div class="col-6 align-self-end text-right">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor quos non natus, doloribus minima expedita quasi repellendus reiciendis dicta placeat perferendis, aspernatur eligendi nam magni rem, possimus accusamus aut nulla.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-xxl d-flex">
            <div class="card illustation flex-fill">
                <div class="card-body p-0 d-flex flex-fill">
                    <div class="row no-gutters w-100">
                        <div class="col-6">
                            <div class="illustration-text p-3 m-l">
                                <h4 class="illustration-text">welcome to page</h4>
                                <p class="bm-0">my new dashboard</p>
                            </div>
                        </div>
                        <div class="col-6 align-self-end text-right">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor quos non natus, doloribus minima expedita quasi repellendus reiciendis dicta placeat perferendis, aspernatur eligendi nam magni rem, possimus accusamus aut nulla.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-xxl d-flex">
            <div class="card illustation flex-fill">
                <div class="card-body p-0 d-flex flex-fill">
                    <div class="row no-gutters w-100">
                         <div class="col-6">
                            <div class="illustration-text p-3 m-l">
                                <h4 class="illustration-text">welcome to page</h4>
                                <p class="bm-0">my new dashboard</p>
                            </div>
                        </div>
                        <div class="col-6 align-self-end text-right">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor quos non natus, doloribus minima expedita quasi repellendus reiciendis dicta placeat perferendis, aspernatur eligendi nam magni rem, possimus accusamus aut nulla.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div-->
    <div class="row" style="margin-top: 10px;">
        <div class="col-12 col-md-6 col-xl d-flex">
            <div class="card flex-fill">
                <div class="card-body py-4 text-center">
                    <!--div class="float-right text-danger">
                        -5.28%
                    </div-->
                    <h5 class="mb-3"><strong>Ventas del dia de hoy</strong></h5>
                    <div class="">
                       <h4><span class="badge bg-secondary"><strong>{{$ventas}}</strong></span</h4>
                    </div>
                    <!--div>
                        volumen: 2,692.47 btc
                    </div-->
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 col-xl d-flex">
            <div class="card flex-fill">
                <div class="card-body py-4 text-center">
                    <!--div class="float-right text-danger">
                        -5.28%
                    </div-->
                    <h5 class="mb-3"><strong>Total de articulos</strong></h5>
                    <div class="">
                       <h4><span class="badge bg-secondary"><strong>{{$productos}}</strong></span</h4>
                    </div>
                    <!--div>
                        volumen: 2,692.47 btc
                    </div-->
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 col-xl d-flex">
            <div class="card flex-fill">
                <div class="card-body py-4 text-center">
                    <!--div class="float-right text-danger">
                        -5.28%
                    </div-->
                    <h5 class="mb-3"><strong>Compras el dia de hoy</strong></h5>
                    <div class="">
                       <h4><span class="badge bg-secondary"><strong>{{$entradas}}</strong></span</h4>
                    </div>
                    <!--div>
                        volumen: 2,692.47 btc
                    </div-->
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 col-xl d-flex">
            <div class="card flex-fill">
                <div class="card-body py-4 text-center">
                    <!--div class="float-right text-danger">
                        -5.28%
                    </div-->
                    <h5 class="mb-3"><strong>Cajas abiertas</strong></h5>
                    <div class="">
                       <h4><span class="badge bg-secondary"><strong>{{$cajas}}</strong></span</h4>
                    </div>
                    <!--div>
                        volumen: 2,692.47 btc
                    </div-->
                </div>
            </div>
        </div>
    </div>
@endsection