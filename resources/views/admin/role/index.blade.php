@extends('layouts.admin')
@section('contenido')
<div class="container"><br>
    <div class="row justify-content-center">
        <div class="card">
        <div class="card-header">
            <strong>Roles</strong>
            <a href="{{url('admin/role/create')}}" class="btn btn-primary float-right">Crear un nuevo Rol</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                @include('custom.message')
                <table class="table table-bordered table-condensed table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Slug</th>
                            <th>Descripción</th>
                            <th>Acceso completo</th>
                            <th colspan="3">Acciones</th>
                        </tr>
                    </thead>
                    <body>
                    
                        @foreach($roles as $role)
                        <tr>
                            <td>{{$role->id}}</td>
                            <td>{{$role->name}}</td>
                            <td>{{$role->slug}}</td>
                            <td>{{$role->description}}</td>
                            <td>{{$role['full-access']}}</td>
                            <td>
                                <a href="{{route('role.show',$role->id)}}" class="btn btn-info btn-sm">Mostrar</a>
                            </td>
                            <td>
                                <a href="{{route('role.edit',$role->id)}}" class="btn btn-success btn-sm">Editar</a>
                            </td>
                            <td>
                                <form action="{{route('role.destroy',$role->id)}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger btn-sm">Eliminar</button>
                                </form>
                            </td>
                        </tr>    
                        @endforeach
                   
                    </body>
                </table>
                {{$roles->links()}}
            </div>
        </div>
        </div>
        
    </div>
</div>
@endsection