@extends('layouts.admin')

@section('contenido')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h2>Editar Rol</h2></div>

                <div class="card-body">
                    @include('custom.message')
                    <form action="{{route('role.update',$role->id)}}" method="post">
                    @csrf
                    @method('put')
                    <div class="container">
                        <h4>Requiere datos</h4>
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" id="name" value="{{old('name',$role->name)}}" placeholder="Nombre" readonly>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control"  name="slug" id="slug" value="{{old('slug',$role->slug)}}" placeholder="slug" readonly>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="description"  id="description" rows="3" placeholder="Descripción" readonly>{{old('description',$role->description)}}</textarea>    
                        </div>
                        <hr>
                        <h4>Acceso completo</h4>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="fullaccessyes" name="full-access" class="custom-control-input" disabled value="yes"
                            @if($role['full-access'] == "yes")
                                checked
                            
                            @elseif(old('full-access') == "yes")
                                checked
                            @endif
                            >
                            <label class="custom-control-label" for="fullaccessyes">Yes</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="fullaccessno" name="full-access" class="custom-control-input" disabled value="no"
                             @if($role['full-access'] == "no")
                                checked
                            
                            @elseif(old('full-access') == "no")
                                checked
                            @endif
                           
                            >
                            <label class="custom-control-label" for="fullaccessno">No</label>
                        </div>
                        <hr>
                        <h4>Lista de permisos</h4>
                        @foreach($permissions as $permission)
                            
                      
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" disabled id="permission_{{$permission->id}}" value="{{$permission->id}}" name="permission[]"
                            @if(is_array(old('permission')) && in_array("$permission->id", old('permission')))
                                checked
                            @elseif(is_array($permission_role) && in_array("$permission->id", $permission_role))
                                checked    
                            @endif
                            >

                            <label class="custom-control-label" for="permission_{{$permission->id}}">{{$permission->id}}-{{$permission->name}}<em> ( {{$permission->description}} ) </em></label>
                        </div>
                        @endforeach
                        <br>
                        {{-- <input type="submit"  value="Guardar"> --}}
                        <a class="btn btn-success" href="{{route('role.edit',$role->id)}}">Editar</a>
                        <a class="btn btn-danger" href="{{route('role.index')}}">Regresar</a>
                    </div>
                    </form>
                    {{-- {!! dd(old()) !!} --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
