@extends('layouts.admin')
@section('contenido')
<section class="margin">
    <div class="card">
        <div class="card-header">
            Crear un Rol
        </div>
        <div class="card-body">
            @include('custom.message')
            <form action="{{route('role.store')}}" method="post" autocomplete="off">
            @csrf
            <div class="row">
                <div class="col-md-3">
                    <div class="group">
                        <label for="">Nombre del rol</label>
                        <input type="text" class="form-control" name="name" id="name" value="{{old('name')}}" placeholder="Nombre, No debe de existir ningun otro rol con este mismo nombre">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="group">
                        <label for="">Nombre del slug</label>
                        <input type="text" class="form-control"  name="slug" id="slug" value="{{old('slug')}}" placeholder="slug, No debe de existir ningun otro rol con este mismo slug">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="group">
                        <label for="">Descripcion del rol</label>
                        <textarea class="form-control"  name="description"  id="description" rows="1" placeholder="Realiza una descripción, ejemplo (El usuario encargado de compras,ventas etc ...)">{{old('description')}}</textarea>  
                    </div>
                </div>
            </div>
            <hr>
            <diw class="row">
                <div class="col-md-12 text-center">
                    <h5><strong>Acceso completo</strong></h5>
                    <small class="form-text text-muted">Si usted checked Si no es necesario checked la lista de permisos</small>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="fullaccessyes" name="full-access" class="custom-control-input" onclick="getvalueradio(this.value);" value="yes"
                        @if(old('full-access') == "yes")
                            checked
                        @endif
                        >
                        <label class="custom-control-label" for="fullaccessyes">Si</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="fullaccessno" name="full-access" class="custom-control-input" onclick="getvalueradio(this.value);" value="no"
                        @if(old('full-access') == "no")
                            checked
                        @endif
                        @if(old('full-access') === null)
                            checked
                        @endif
                        >
                        <label class="custom-control-label" for="fullaccessno">No</label>
                    </div>
                </div>
            </diw>
        </div>
    </div>
</section>
<div class="div_roles_permisos">
    <section style="margin-top: -3px;">
        <div class="card-group">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Menu principal</h5>
                    @foreach($permissions as $permission)
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="permission_{{$permission->id}}" value="{{$permission->id}}" name="permission[]"
                        @if(is_array(old('permission')) && in_array("$permission->id", old('permission')))
                            checked
                        @endif
                        >
                        <label class="custom-control-label" for="permission_{{$permission->id}}">{{$permission->id}}-{{$permission->name}}<em> ( {{$permission->description}} )</em></label>
                    </div>
                    @endforeach
                </div>
                <div class="card-footer">
                <small class="text-muted">Roles y permisos</small>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Menu caja</h5>
                    @foreach($permission_caja as $caja)
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="permission_{{$caja->id}}" value="{{$caja->id}}" name="permission[]"
                        @if(is_array(old('permission')) && in_array("$caja->id", old('permission')))
                            checked
                        @endif
                        >
                        <label class="custom-control-label" for="permission_{{$caja->id}}">{{$caja->id}}-{{$caja->name}}<em> ( {{$caja->description}} )</em></label>
                    </div>
                    @endforeach
                </div>
                <div class="card-footer">
                <small class="text-muted">Roles y permisos</small>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Menu almacen</h5>
                    @foreach($permission_almacen as $almacen)
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="permission_{{$almacen->id}}" value="{{$almacen->id}}" name="permission[]"
                        @if(is_array(old('permission')) && in_array("$almacen->id", old('permission')))
                            checked
                        @endif
                        >
                        <label class="custom-control-label" for="permission_{{$almacen->id}}">{{$almacen->id}}-{{$almacen->name}}<em> ( {{$almacen->description}} )</em></label>
                    </div>
                    @endforeach
                </div>
                <div class="card-footer">
                <small class="text-muted">Roles y permisos</small>
                </div>
            </div>
        </div>
    </section>
    <section class="margin">
        <div class="card-group">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Menu compras</h5>
                    @foreach($permission_compras as $compras)
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="permission_{{$compras->id}}" value="{{$compras->id}}" name="permission[]"
                        @if(is_array(old('permission')) && in_array("$compras->id", old('permission')))
                            checked
                        @endif
                        >
                        <label class="custom-control-label" for="permission_{{$compras->id}}">{{$compras->id}}-{{$compras->name}}<em> ( {{$compras->description}} )</em></label>
                    </div>
                    @endforeach
                </div>
                <div class="card-footer">
                <small class="text-muted">Roles y permisos</small>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Menu ventas</h5>
                    @foreach($permission_ventas as $ventas)
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="permission_{{$ventas->id}}" value="{{$ventas->id}}" name="permission[]"
                        @if(is_array(old('permission')) && in_array("$ventas->id", old('permission')))
                            checked
                        @endif
                        >
                        <label class="custom-control-label" for="permission_{{$ventas->id}}">{{$ventas->id}}-{{$ventas->name}}<em> ( {{$ventas->description}} )</em></label>
                    </div>
                    @endforeach
                </div>
                <div class="card-footer">
                <small class="text-muted">Roles y permisos</small>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Menu devolucion</h5>
                    @foreach($permission_devolucion as $devolucion)
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="permission_{{$devolucion->id}}" value="{{$devolucion->id}}" name="permission[]"
                        @if(is_array(old('permission')) && in_array("$devolucion->id", old('permission')))
                            checked
                        @endif
                        >
                        <label class="custom-control-label" for="permission_{{$devolucion->id}}">{{$devolucion->id}}-{{$devolucion->name}}<em> ( {{$devolucion->description}} )</em></label>
                    </div>
                    @endforeach
                </div>
                <div class="card-footer">
                <small class="text-muted">Roles y permisos</small>
                </div>
            </div>
        </div>
    </section>

    <section class="margin">
        <div class="row">
            <div class="col">
                <div class="card h-100">
                <div class="card-body">
                    <h5 class="card-title">Menu inventario</h5><br>
                    @foreach($permission_inventario as $inventario)
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="permission_{{$inventario->id}}" value="{{$inventario->id}}" name="permission[]"
                        @if(is_array(old('permission')) && in_array("$inventario->id", old('permission')))
                            checked
                        @endif
                        >
                        <label class="custom-control-label" for="permission_{{$inventario->id}}">{{$inventario->id}}-{{$inventario->name}}<em> ( {{$inventario->description}} )</em></label>
                    </div>
                    @endforeach
                </div>
                </div>
            </div>
        </div>
    </section>
</div><!--fin de contenido roles y permisos-->
    <section class="margin">
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <div class="row text-center">
                        <div class="col-md-12">
                        <input type="submit" class="btn btn-primary" value="Guardar">
                        <a class="btn btn-danger" href="{{route('role.index')}}">Regresar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </section>
<style>
.margin{
    margin-top: 5px;
}
</style>
@push('ScriptRoleCreate')
<script src="{{asset('js/funciones_role/role_create.js')}}"></script>
@endpush
@endsection
