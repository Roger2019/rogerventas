@extends('layouts.admin')
@section('contenido')
<section class="margin">
    <div class="card">
        <div class="card-header">
            <h6><strong>Administrar el stock de los productos</strong></h6> 
        </div>
        <div class="card-body">
            <form id="updateproductstock">
                <div class="table-responsive tab-scroll">
                    <table class="table table-bordered table-sm"> 
                        <thead>
                            <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nombre del articulo</th>
                            <th scope="col">Stock actual</th>
                            <th scope="col">Nuevo stock</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($products as $pro)
                            <tr>
                                <th style="width:5%;" scope="row"><input type="text" name="art[]" class="stock_actual" value="{{$pro->articulo_id}}" hidden="true"> {{$pro->idcaptura}}</th>
                                <td>{{$pro->nombre}}</td>
                                <td>{{$pro->stock}}</td>
                                <td style="width:20%"><input type="text" name="newstock[]" class="form-control input-sm" value="{{$pro->cantidad}}" onkeypress="return filterFloat(event,this);"></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
        <div id="errors" class="col-md-6 offset-md-2">
        </div>
        <div class="card-footer text-muted" style="border:1px solid#D3D3D3;">
            <div class="row col-md-2">
                <button id="updatenewstock" class="btn btn-secondary btn-block">Actualizar</button>
            </div>
        </div>
    </div>
</section>
<style>
.margin{
    margin-top: 5px;
}
.tab-scroll{
  /*background-color: lightblue;*/
  /*background-color: #d9e6fb;*/
  /*background-color: #d6d8db;*/
  height: 394px;
  overflow-y: scroll; 
  /*color: white;*/
  
}
.input-sm{
  box-sizing: border-box;
  border: 3px solid #ccc;
  -webkit-transition: 0.5s;
  transition: 0.5s;
  outline: none; 
}
.input-sm:focus{
 border: 3px solid #555;
}
</style>
@push('ScriptAjustingInventory')
    <script src="js/funciones_inventario/ajustar_inventario.js"></script>
@endpush
@endsection