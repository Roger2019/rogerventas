@extends('layouts.admin')
@section('contenido')
    <section style="margin-top:5px;">
        <div class="card">
            <div class="card-body">
                <div class="group">
                    <label for="">Selecciona lactegoria</label>
                    <select name="" id="select_categoria" class="form-select">
                        <option value="">Selecciona la categoria</option>
                        @foreach ($categorias as $item)
                            <option value="{{$item->idcategoria}}">{{$item->nombre}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </section>
    <section style="margin-top:-9px;">
        <div class="card" style="">
            <!--div class="card-header">
                <h5 class="card-header">Productod de la categoria</h5>
            </div-->
           {{-- input containing the number of records--}}
            <input type="text" id="art-reg" hidden="true">
            <div class="card-body">
                <form id="form-add-cantidad" autocomplete="off">
                    <div class="row">
                        <div class="col-md-12 table-responsive" id="show_table_art">
                            <table class="table table-bordered table-sm" style="">
                                <thead>
                                    <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Codigo</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">stock</th>
                                    <th scope="col">cantidad</th>
                                    <!--th scope="col">acciones</th-->
                                    </tr>
                                </thead>
                                <tbody id="idbodyart">
                                </tbody>
                            </table>
                        </div>
                        <!--progerrbarr-->
                        <div class="container" id="progressbar">
                        </div>
                    </div>
                </form>
            </div>
            <div id="error-ms" class="col-md-5 offset-md-3">
            </div>
            <div class="card-footer text-muted" style="border:1px solid#D3D3D3;">
                <button id="savecantidad" class="btn btn-secondary">Aceptar</button>
            </div>
        </div>
    </section>
    <template id="template_show_articles">
        <tr>
            <th class="art-consecutivo"></th>
            <td class="art-codigo"></td>
            <td  style="width:50%"><p class="art-name"></p><input type="text" name="art[]" class="art-id" hidden="true"></td>
            <td class="art-stock" style="width:10%"></td>
            <td class="" style="width:20%"><input type="text" name="insertcantprod[]" class="form-control input-sm" onkeypress="return filterFloat(event,this);"></td>
            <!--td class="art-"><button>Agregar</button></td-->
        </tr>
    </template>
    <style>
        .input-sm{
            box-sizing: border-box;
            border: 3px solid #ccc;
            -webkit-transition: 0.5s;
            transition: 0.5s;
            outline: none; 
        }
        .input-sm:focus{
            border: 3px solid #555;
        }
    </style>
@push('Scriptinventarioindex')
  <script src="js/funciones_inventario/inventario_index.js"></script>  
@endpush
@endsection